//
// Starting code for the MPI coursework.
//
// See lectures and/or the worksheet corresponding to this part of the module for instructions
// on how to build and launch MPI programs. A simple makefile has also been included (usage optional).
//


//
// Includes.
//

// Standard includes.
#include <stdio.h>
#include <stdlib.h>

// The MPI library.
#include <mpi.h>

// Some extra routines for this coursework. DO NOT MODIFY OR REPLACE THESE ROUTINES,
// as this file will be replaced with a different version for assessment.
#include "cwk2_extra.h"


//
// Main.
//
int main( int argc, char **argv )
{
    //int i;

    //
    // Initialisation.
    //

    // Initialise MPI and get the rank of this process, and the total number of processes.
    int globalRank, localRank;
    MPI_Init( &argc, &argv );
    MPI_Comm nodeComm, masterComm;

    MPI_Comm_rank( MPI_COMM_WORLD, &globalRank     );
    MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, globalRank, MPI_INFO_NULL, &nodeComm);
    MPI_Comm_rank( nodeComm, &localRank);
    MPI_Comm_split( MPI_COMM_WORLD, localRank, globalRank, &masterComm );
    MPI_Comm_free( &nodeComm );

    if ( localRank == 0 ) {
        // Now, each process of masterComm is on a different node
        // so you can play with them to do what you want
        int mRank, mSize;
        MPI_Comm_rank( masterComm, &mRank );
        MPI_Comm_size( masterComm, &mSize );
        // do something here
        char name[MPI_MAX_PROCESSOR_NAME];
        int len;
        MPI_Get_processor_name( name, &len );
        printf( "Node number %d/%d is %s\n", mRank, mSize, name );
    }

    // Check that the number of processes is a power of 2, but <=256, so the data set, which is a multiple of 256 in length,
    // is also a multiple of the number of processes. If using OpenMPI, you may need to add the argument '--oversubscribe'
    // when launnching the executable, to allow more processes than you have cores.

   

    

    
    


    //
    // Output the results alongside a serial check.
    //
    

    //
    // Free all resources (including any memory you have dynamically allocated), then quit.
    //

    MPI_Finalize();

    return EXIT_SUCCESS;
}
