//
// Starting point for the OpenCL coursework for COMP3221 Parallel Computation.
//
// Once compiled, execute with the number of rows and columns for the matrix, e.g.
//
// ./cwk3 16 8
//
// This will display the matrix, followed by another matrix that has not been transposed
// correctly. You need to implement OpenCL code so that the transpose is correct.
//
// For this exercise, both the number of rows and columns must be a power of 2,
// i.e. one of 1, 2, 4, 8, 16, 32, ...
//


//
// Includes.
//
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// For this coursework, the helper file has 3 routines in addition to simpleOpenContext_GPU() and compileKernelFromFile():
// - getCmdLineArgs(): Gets the command line arguments and checks they are valid.
// - displayMatrix() : Displays the matrix, or just the top-left corner if it is too large.
// - fillMatrix()    : Fills the matrix with random values.
// Do not alter these routines, as they will be replaced with different versions for assessment.
#include "helper_cwk.h"


// modified displaymatrix to highlight errors
//
// Displays the matrix, or part of it if the matrix is too large for a typical shell window.
//
void displayErrorMatrix( float *M, int n, int m )
{
    int i, j;

    // Display the matrix. Only display the top-left corner if large, with ellipses '...' to suggest continuation.
    for( i=0; i<(n<16?n:16); i++ )
    {
        for( j=0; j<(m<16?m:16); j++ ) {
            if (M[i*m+j] >= 0.0f) printf( "%.3f ", M[i*m+j] );
            else printf(" xxx ");
        }
        if( m>16 ) printf( ". . ." );
        printf( "\n" );
    }

    // Ellipses at the bottom of the matrix.
    if( n>16 )
        for( i=0; i<3; i ++ )
        {
            for( j=0; j<(m<16?m:16); j++ ) printf( "  .   " );
            printf( "\n" );
        }

    printf( "\n" );
}
//

//
// Main.
//
int main( int argc, char **argv )
{
    //
    // Parse command line arguments and check they are valid. Handled by a routine in the helper file.
    //
    int nRows, nCols;
    getCmdLineArgs( argc, argv, &nRows, &nCols );

    //
    // Initialisation.
    //

    // Set up OpenCL using the routines provided in helper_cwk.h.
    cl_device_id device;
    cl_context context = simpleOpenContext_GPU(&device);

    // Open up a single command queue, with the profiling option off (third argument = 0).
    cl_int status;
    cl_command_queue queue = clCreateCommandQueue( context, device, 0, &status );

    // Allocate memory for the matrix.
    float *hostMatrix = (float*) malloc( nRows*nCols*sizeof(float) );

    // Allocate memory for the final matrix.
    float *finalMatrix = (float*) malloc( nRows*nCols*sizeof(float) );

    // Fill the matrix with random values, and display.
    fillMatrix( hostMatrix, nRows, nCols );
    printf( "Original matrix (only top-left shown if too large):\n" );
    displayMatrix( hostMatrix, nRows, nCols );


    /// Student work begins here
    // Transpose the matrix on the GPU.
    //

    // min max dimensions
    int minD = nRows;
    int maxD = nCols;
    if (nRows > nCols) minD = nCols, maxD = nRows;

    // create dimensions array
    int dimensions[2] = {minD, maxD};

    //find max work group size
    size_t maxWorkItems;
    clGetDeviceInfo(device, CL_DEVICE_MAX_WORK_GROUP_SIZE, sizeof(size_t), &maxWorkItems, NULL);
    float groupMod = (float)maxD / (float)maxWorkItems;

    if (groupMod <= 1.0f) groupMod = 1.0f;


    cl_mem device_dimensions = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, 2*sizeof(int), dimensions, &status);
    cl_mem device_groupMod = clCreateBuffer(context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(float), &groupMod, &status);
    cl_mem device_matrix = clCreateBuffer(context, CL_MEM_READ_WRITE | CL_MEM_COPY_HOST_PTR, nRows*nCols*sizeof(float), hostMatrix, &status);
    

    // build kernel
    cl_kernel kernel = compileKernelFromFile("cwk3.cl", "transpose", context, device);

    status = clSetKernelArg(kernel, 0, sizeof(cl_mem), &device_dimensions);
    status = clSetKernelArg(kernel, 1, sizeof(cl_mem), &device_groupMod);
    status = clSetKernelArg(kernel, 2, sizeof(cl_mem), &device_matrix);

    

    // problem size and work group size
    size_t indexSpaceSize[1], workGroupSize[1];
    indexSpaceSize[0] = nRows * nCols;
    workGroupSize[0] = maxD / groupMod;


    // enqueue tasks
    status = clEnqueueNDRangeKernel(queue, kernel, 1, NULL, indexSpaceSize, workGroupSize, 0, NULL, NULL);
    if (status != CL_SUCCESS) {
        printf("failed to enqueue to kernel: Error %d.\n", status);
        return EXIT_FAILURE;
    }

    // read results back 
    status = clEnqueueReadBuffer(queue, device_matrix, CL_TRUE, 0, nRows*nCols*sizeof(float), finalMatrix, 0, NULL, NULL);
    if (status != CL_SUCCESS) {
        printf("could not copy device data to host: Error %d.\n", status);
        return EXIT_FAILURE;
    }
    

    //
    // Display the final result. This assumes that the transposed matrix was copied back to the hostMatrix array
    // (note the arrays are the same total size before and after transposing - nRows * nCols - so there is no risk
    // of accessing unallocated memory).
    //
    printf( "Transposed matrix (only top-left shown if too large):\n" );
//    printf("[%f, %f,\n %f, %f,\n %f, %f,\n %f, %f]\n", finalMatrix[0],finalMatrix[1],finalMatrix[2],finalMatrix[3],
//                                                   finalMatrix[4],finalMatrix[5],finalMatrix[6],finalMatrix[7]);
    displayMatrix( finalMatrix, nCols, nRows );

    // test in serial to verify 
    int issues = 0;
    int doublecopies = 0;
    int i,j;
    for (i = 0; i < nRows; i++) {
        for (j = 0; j < nCols; j++) {
            float test = finalMatrix[j * nRows + i];
            if (fabsf((test - floorf(test)) - hostMatrix[i * nCols + j]) > 0.001) {
                issues ++;
                finalMatrix[j*nRows+i] *= -1.0f;
            }
        }
    }
    if (issues == 0 && doublecopies == 0) {
        printf("Transpose is correct!\n");
    } else {
        printf ("Found %i issues out of %i entries \n %f percent error \n => %i doublecopies found \n", issues, nRows * nCols, 100.0 * (double)issues/(double)(nRows * nCols), doublecopies);
        displayErrorMatrix(finalMatrix, nCols, nRows);
    }


    //
    // Release all resources.
    //
    clReleaseMemObject(device_matrix);

    clReleaseCommandQueue( queue   );
    clReleaseContext     ( context );

    free( hostMatrix );
    free( finalMatrix);

    return EXIT_SUCCESS;
}

