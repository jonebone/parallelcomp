// Kernel for matrix transposition.
__kernel
void transpose( __constant int *dimensions, __constant float *groupMod, __global float *matrix) {
    int startIndex, newIndex, i, j, minD, maxD;
    minD = dimensions[0], maxD = dimensions[1];
    if (groupMod[0] == 1.0f) {
        i = get_group_id(0);
        j = get_local_id(0);
    }
    else {
        i = floor(get_group_id(0) / groupMod[0]);
        j = fmod(get_group_id(0), groupMod[0]) * get_local_size(0) + get_local_id(0);
    } 
    // indices to swap
    // swapping works only for square matrix
    startIndex = i * maxD + j;
    
    newIndex = j * minD + i;

    // if indices are the same no change will occur
    // if newIndex is less than startIndex it will be swapped when startIndex < newIndex
    if (newIndex <= startIndex) return;

    float temp = matrix[startIndex];
    matrix[startIndex] = matrix[newIndex];
    matrix[newIndex] = temp;
}