Complete the table below with your results, and then provide your interpretation at the end.

Note that:

- When calculating the parallel speed-up S, use the time output by the code, which corresponds
  to the parallel calculation and does not include reading in the file or performing the serial check.

- Take as the serial execution time the time output by the code when run with a single process
  (hence the speed-up for 1 process must be 1.0, as already filled in the table).


No. Process:                        Mean time (average of 5 runs)           Parallel speed-up, S:
===========                         ============================:           ====================
1                                    0.5744 ms                                  1.0
2                                    0.5356 ms                                  1.072
4                                    0.5714 ms                                  1.005

Architecture that the timing runs were performed on:
    MacBook Pro, hexcore i7s 
    running Mojave 10.14.6 
    mpich version 3.4.1

A brief interpretation of these results (2-3 sentences should be enough):
    It makes sense that we start to see diminishing returns from higher numbers of processes,
since the number of levels in our binary tree is half the number of processes, and from 2 process
to 4 process we go from 1 send/receive pair in 1 level to having 2 send/receive pairs in 1 level
and 1 more send/receive pair in the next. It is worth noting that the time increase due to the
larger tree is much more than just the difference between the times, since theoretically the 
operation done by each process would have taken 4 processes half as long as it would have taken
2 processes. Were each process to be doing a more costly operation than summing an array, the time 
taken by the binary tree to reduce the many processes to a single result may be more worthwhile.
    
    it is worth stating that I ran my all tests with the option -ppn 6 to ensure that with a node
set to have 6 process, there would be no lag due to inter-node communication