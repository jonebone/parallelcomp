//
// Starting code for the MPI coursework.
//
// See lectures and/or the worksheet corresponding to this part of the module for instructions
// on how to build and launch MPI programs. A simple makefile has also been included (usage optional).
//


//
// Includes.
//

// Standard includes.
#include <stdio.h>
#include <stdlib.h>

// The MPI library.
#include <mpi.h>

// Some extra routines for this coursework. DO NOT MODIFY OR REPLACE THESE ROUTINES,
// as this file will be replaced with a different version for assessment.
#include "cwk2_extra.h"


//
// Main.
//
int main( int argc, char **argv )
{
    int i;

    //
    // Initialisation.
    //

    // Initialise MPI and get the rank of this process, and the total number of processes.
    int rank, numProcs;
    MPI_Init( &argc, &argv );
    MPI_Comm_size( MPI_COMM_WORLD, &numProcs );
    MPI_Comm_rank( MPI_COMM_WORLD, &rank     );

    // Check that the number of processes is a power of 2, but <=256, so the data set, which is a multiple of 256 in length,
    // is also a multiple of the number of processes. If using OpenMPI, you may need to add the argument '--oversubscribe'
    // when launnching the executable, to allow more processes than you have cores.
    if( (numProcs&(numProcs-1))!=0 || numProcs>256 )
    {
        // Only display the error message from one processes, but finalise and quit all of them.
        if( rank==0 ) printf( "ERROR: Launch with a number of processes that is a power of 2 (i.e. 2, 4, 8, ...) and <=256.\n" );

        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // Load the full data set onto rank 0.
    float *globalData = NULL;
    int globalSize = 0;
    if( rank==0 )
    {
        globalData = readDataFromFile( &globalSize );           // globalData must be free'd on rank 0 before quitting.
        if( globalData==NULL )
        {
            MPI_Finalize();                                     // Should really communicate to all other processes that they need to quit as well ...
            return EXIT_FAILURE;
        }

        printf( "Rank 0: Read in data set with %d floats.\n", globalSize );
    }

    // Calculate the number of floats per process. Note that only rank 0 has the correct value of localSize
    // at this point in the code. This will somehow need to be communicated to all other processes. Note also
    // that we can assume that globalSize is a multiple of numProcs.
    int localSize = globalSize / numProcs;          // = 0 at this point of the code for all processes except rank 0.

    // Start the timing now, after the data has been loaded (will only output on rank 0).
    double startTime = MPI_Wtime();

    //
    // Preperation work: setting up for tasks 1 and 2
    //
    // broadcast localSize so that all processes have it
    MPI_Bcast(&localSize, 1, MPI_INT, 0, MPI_COMM_WORLD);
    // initialize localData arrays now that every process has localSize
    float* localData = (float*) malloc(localSize * sizeof(float));
    // errorcheck
    if (!localData) {
        printf("could not allocate memory for localData at rank %d , localSize was %i \n", rank, localSize);
        MPI_Finalize();
        return EXIT_FAILURE;
    }

    // scatter globalData to localData
    // only one scatter is needed for both tasks, since neither task modifies the values in localData
    MPI_Scatter(
        globalData, localSize, MPI_FLOAT,
        localData,  localSize, MPI_FLOAT,
        0, MPI_COMM_WORLD
    );

    //
    // Task 1: Calculate the mean using all available processes.
    //
    float mean;
    float sum = 0.0f;
    for (i = 0; i < localSize; i++) sum += localData[i];
    mean = sum / localSize;
    
    // use reduce to combine the means from each process into sum at rank 0
    MPI_Reduce(&mean, &sum, 1, MPI_FLOAT, MPI_SUM, 0, MPI_COMM_WORLD);

    // at rank 0: divide sum of local means by number of processess
    if (rank == 0) mean = sum / numProcs;

    //
    // Task 2. Calculate the variance using all processes.
    //
    float variance;
    // broadcast the mean from rank 0 to all processes
    MPI_Bcast(&mean, 1, MPI_FLOAT, 0, MPI_COMM_WORLD);
    // reset sum
    sum = 0.0f;
    for (i = 0; i < localSize; i++) sum += (localData[i] - mean) * (localData[i] - mean);
    variance = sum / localSize;



    // initialize status for recv
    MPI_Status status;
    // point-to-point binary tree style reduction
    // makes sense to use blocking communication, no calculations to be done until we have received the sent data
    for (i = 1; i <= numProcs / 2; i ++) {
        // ranks divisible by 2 * i
        // rank 0 will always be receiving
        if ((rank & (2 * i - 1)) == 0) {
            MPI_Recv(&sum, 1, MPI_FLOAT, rank + i, 0, MPI_COMM_WORLD, &status);
            sum += variance;
            variance = sum * 0.5;
        }
        // ranks that are i away from being divisble by 2*i, e.g. (1, 3, 5,...), then (2, 6, 10,...) and so on
        else if (((rank + i) & (2 * i - 1)) == 0) MPI_Send(&variance, 1, MPI_FLOAT, rank - i, 0, MPI_COMM_WORLD);
    }

    //
    // Output the results alongside a serial check.
    //
    if( rank==0 )
    {
        // Output the results of the timing now, before moving onto other calculations.
        printf( "Total time taken: %g s\n", MPI_Wtime() - startTime );

        // Your code MUST call this function after the mean and variance have been calculated using your parallel algorithms.
        // Do not modify the function itself (which is defined in 'cwk2_extra.h'), as it will be replaced with a different
        // version for the purpose of assessing. Also, don't just put the values from serial calculations here or you will lose marks.
        finalMeanAndVariance( mean, variance );
            // You should replace the first argument with your mean, and the second with your variance.

        // Check the answers against the serial calculations. This also demonstrates how to perform the calculations
        // in serial, which you may find useful. Note that small differences in floating point calculations between
        // equivalent parallel and serial codes are possible, as explained in Lecture 11.

        // Mean.
        float sum = 0.0;
        for( i=0; i<globalSize; i++ ) sum += globalData[i];
        float mean = sum / globalSize;

        // Variance.
        float sumSqrd = 0.0;
        for( i=0; i<globalSize; i++ ) sumSqrd += ( globalData[i]-mean )*( globalData[i]-mean );
        float variance = sumSqrd / globalSize;

        printf( "SERIAL CHECK: Mean=%g and Variance=%g.\n", mean, variance );

   }

    //
    // Free all resources (including any memory you have dynamically allocated), then quit.
    //
    if( rank==0 ) free( globalData );
    free(localData);

    MPI_Finalize();

    return EXIT_SUCCESS;
}
