
#include <stdio.h>
#include <stdlib.h>

int main( int argc, char **argv ) {
    int lines = 1000;
    
    const char * path = "testData.txt";
    const char * mode = "w";

    FILE * f;
    f = fopen(path, mode);

    fprintf(f, "%i\n", lines);
    for (int i = 0; i < lines; i ++) {
        int r = rand() % 10;
        fprintf(f,"a %i\n", lines * r + i);
    }

    fclose(f);
    return 0;
}