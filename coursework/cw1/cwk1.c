//
// COMP3221 Parallel Computation: OpenMP.
//


//
// Includes.
//

// Standard includes.
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// The OMP library.
#include <omp.h>

// The include file for this coursework. You may inspect this file, but should NOT alter it, as it will be replaced
// with a different version for assessment.
#include "cwk1_extra.h"

// Key variables and functions in "cwk1_extra.h":
// - each entry in the data array is one of the following structs:
//   typedef struct Entry
//   {
//     char *name;
//     int id;
//   } Entry_t;
// - the pointer to the array, and the number of entries within it, are global:
//  Entry_t *orderedData;
//  int dataSize;
//
// int loadOrderedData() loads the data from "orderedData.txt" and returns 0 for success.
// void printData() displays the full data array to stdout.
// void deleteOrderedData() performs all required operations on the data array before quitting program.
//
// void swapEntries( int i, int j )
// - swaps the entries with indices i and j. Prints an error message if i==j, or if either index is out of range.
//   Uses local temporary variables. Not thread safe.
//
// int randomEntryIndex()
// - returns a random entry index, i.e. a random integer in the range 0 to dataSize-1 inclusive. Not thread safe.


//
// The following 4 functions correspond to each of the operations. It is expected you will add fill in each of these functions
// with your solution. You can also add other functions to this file if you like, but if you decide to add a new file, make
// sure to follow the coursework instructions for submission.
//

// Reverses the order of the data in-place, i.e. leaving the answer in the same array as before.
void reverseOrder_inParallel() {
    int i;
    #pragma omp parallel for
    for (i = 1; i <= dataSize / 2; i++) swapEntries(i - 1, dataSize - i);
}

// does one iteration of parallel bubble sort
void bubbleIteration(int offset, int * sorted) {
    int o;
    #pragma omp parallel for
    for (o = offset; o < dataSize - 1; o += 2) if(orderedData[o].id > orderedData[o + 1].id) {
        * sorted = 0; // if any swap needs to be done, it is not yet sorted
        swapEntries(o, o + 1);
    }
}

void bubbleSort() {
    int sorted = 0;
    while (sorted == 0) {
        sorted = 1; // will be changed to 0 unless no swaps need doing
        // swaps pairs (as appropriate) starting with 0,1 then 2,3 then 4,5 etc.
        bubbleIteration(0, &sorted);
        sorted = 1; // will be changed to 0 unless no swaps need doing
        // swaps pairs (as appropriate) starting with 1,2 then 3,4 then 5,6 etc. 
        bubbleIteration(1, &sorted);
    }
}
// bubbleSort, when tested on test datasets of 1000 entries, consistantly ran in over a second
// the below approach, when tested on the same datasets, consistantly took less than 10ms

// returns what the index of the Entry at orderedData[dataIndex] should be when sorted by ID
// ID numbers are assumed to be unique, because what good would they be if they weren't
int sortIndex(int dataIndex) {
    int id = orderedData[dataIndex].id;
    int newIndex = 0;
    int i;
    for (i = 0; i < dataSize; i++) if(id > orderedData[i].id) {
        #pragma omp atomic
        ++newIndex; 
        // this atomic part will be run 2n - 1 times for a single sort
        // since the function will be run n times,
        // and the atomic part will be run 0..n times per function call,
        // and every number of atomic calls will be a unique number 0..n
    }
    return newIndex;
}

// Sort all entries in-place, in order of increasing id.
void sortByID_inParallel() {
    Entry_t *temp = (Entry_t*) malloc(dataSize * sizeof(Entry_t));
    int i;
    #pragma omp parallel for
    for (i = 0; i < dataSize; i++) temp[sortIndex(i)] = orderedData[i]; 
    
    #pragma omp parallel for
    for (i = 0; i < dataSize; i++) orderedData[i] = temp[i];

    free(temp);
}

// this might be less naive but doesn't stay as true to the original intentions as the other implementation
void shuffle() {
    int halfData = (dataSize - 1)/2;
    int offsetA;
    int offsetB;
    int oddBalance;
    int swapIndex;
    int i;
    for (i = 0; i < dataSize; i++) {
        offsetA = i + rand();
        offsetB = i + rand();
        oddBalance = (i % 2) * (dataSize % 2);
        int j;
        #pragma omp parallel for
        for (j = 0; j < halfData; j++) {
            swapIndex = dataSize - (((j + offsetB) % halfData) + oddBalance + 1);
            swapEntries((j + offsetA) % halfData, swapIndex);
        }
    }
}

// Shuffle all items in parallel. Here, "shuffle" means to randomly select 2 entry indices, and swap the corresponding entries.
// This should be done dataSize*(dataSize-1)/2 times so that each pairing is swapped on average once.
void shuffle_inParallel() {
    int halfData = (dataSize - 1)/2;
    unsigned int seed = rand();
    int i;
    #pragma omp parallel for private(seed)
    for (i = 0; i < dataSize * halfData; i++) {
        seed *= i; // maker each seed different
        // rand_r is threadsafe provided seed is private to each thread
        // which is the case, thanks to private(seed)
        int index1 = rand_r(&seed) % dataSize;
        int index2 = rand_r(&seed) % dataSize;
        if (index1 == index2) index2 = (index2 + 1) % dataSize;
        
        #pragma omp critical 
        {
            //printf("%i : (%i <=> %i) \n", i, index1, index2);
            swapEntries(index1, index2);
        }
    }
}

// Remove the last item from the list in a thread-safe manner. You do not need to re-allocate any memory for the data array.
void removeLastItem_threadSafe() {
    #pragma omp atomic
    -- dataSize;
}


//
// You should not modify the code in main(), but should understand how it works.
//
int main( int argc, char **argv ) {
    // Initialise the random number generator to the system clock.
    srand( time(NULL) );

    //
    // Parse command line arguments. Requires an option number to be entered.
    //

    // Make sure we have exactly 1 command line argument (which, plus the executable name, means 'argc' should be exactly 2).
    if( argc != 2 )
    {
        printf( "Enter a single command line argument for the operation required:\n(1) Reverse the order.\n(2) Sort in order of increasing ID.\n" );
        printf( "(3) Shuffle.\n(4) Remove all items from the end in a parallel loop.\n" );
        return EXIT_FAILURE;
    }

    // Convert to an option number, and ensure it is in the valid range. Note argv[0] is the executable name.
    int option = atoi( argv[1] );
    if( option<=0 || option>4 )
    {
        printf( "Option number '%s' invalid.\n", argv[1] );
        return EXIT_FAILURE;
    }

    // Display how many threads we are using, if only to confirm this is actually in parallel.
    printf( "Performing option '%i' using %i OpenMP thread(s).\n\n", option, omp_get_max_threads() );

    //
    // Initialise the data set.
    //

    // Loads the data from file. loadOrderedData() (defined in "cwk1_extra.h" returns a non-negative integer
    // if successful, otherwise it will display an error message and return a negative integer.
    if( loadOrderedData()<0 ) return EXIT_FAILURE;

    // Print the initial ordered data to screen. printData() is defined in "cwk1_extra.h".
    printf( "Before the operation:\n" );
    printData();

    //
    // Perform an operation on the data depending on the option entered on the command line.
    //
    int i, initialDataSize = dataSize;

    switch( option )
    {
        case 1:
            reverseOrder_inParallel();
            break;
        
        case 2:
            sortByID_inParallel();
            break;

        case 3:
            shuffle_inParallel();
            break;

        case 4:
            #pragma omp parallel for
            for( i=0; i<initialDataSize; i++ )
                removeLastItem_threadSafe();
            break;

        default:

            // Shouldn't be possible to reach here given the earlier checks.
            printf( "Option '%i' not implememnted by the switch() statement.\n", option );
            return EXIT_FAILURE;
    }

    //
    // Print the data after the operation, then free up all resources and quit.
    //
    printf( "\nAfter the operation:\n" );
    printData();

    // You MUST call this function from "cwk1_extra.h" (which should be unmodifed) just prior to quitting your program.
    deleteOrderedData();

    return EXIT_SUCCESS;
}
